from data_model.formula_model import *
import json

# modeling of original model
model = odeint(SEIR_model, [S, E, I, Severe, Crit, R, D], t,
               args=(beta, latent, gamma, m_a, gamma_crit,
                     death_rate_normal, death_rate_crit, c_a, N, death_rate_severe))

# Plot Traces for Original Model
S = [elem[0] for elem in model]
E = [elem[1] for elem in model]
I = [elem[2] for elem in model]
Sev = [elem[3] for elem in model]
Crit = [elem[4] for elem in model]
R = [elem[5] for elem in model]
D = [elem[6] for elem in model]

S_trace = get_trace_scatter(dates, S, name='Susceptible', color='MediumBlue')
E_trace = get_trace_scatter(dates, E, name='Exposed', color='LightSkyBlue')
I_trace = get_trace_scatter(dates, I, name='Infected', color='GreenYellow')
Sev_trace = get_trace_scatter(dates, Sev, name='Severe', color='Orange')
Crit_trace = get_trace_scatter(dates, Crit, name='Critical', color='Red')
R_trace = get_trace_scatter(dates, R, name='Recovered', color='Pink')
D_trace = get_trace_scatter(dates, D, name='Deaths', color='Darkred')

traces = [S_trace, E_trace, I_trace,
          Sev_trace, Crit_trace, R_trace, D_trace]

figure = go.Figure(data=traces, layout=layout)
figure.update_layout(title='COVID19 | S-E-I-R epidemic model | Original model',
                     xaxis=dict(
                         rangeslider=dict(
                             visible=True
                         )))


def original_plot():
    graphJSON = json.dumps(figure, cls=py.utils.PlotlyJSONEncoder)

    return graphJSON

# py.offline.plot(figure, auto_open=True, show_link=False)
