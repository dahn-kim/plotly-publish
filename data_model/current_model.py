from data_model.formula_model import *
import json

# beta value for social distancing
def beta_var(t):
    if t > intervention_day and t < (intervention_day + 7):
        return beta * (1 - intervention_rate)
    elif t >= (intervention_day + 7) and t < (intervention_day + 16):
        return beta * (1 - intervention_rate_later)
    elif t >= (intervention_day + 16):
        return beta * (1 - intervention_rate_latest)
    else:
        return beta


# modelling of social distancing
model_interv = odeint(SEIR_model, [S, E, I, Severe, Crit, R, D], t,
                      args=(beta_var, latent, gamma, m_a,
                            gamma_crit, death_rate_normal,
                            death_rate_crit, c_a, N, death_rate_severe))

# Plot Traces for Intervention Model
S_interv = [elem[0] for elem in model_interv]
E_interv = [elem[1] for elem in model_interv]
I_interv = [elem[2] for elem in model_interv]
Sev_interv = [elem[3] for elem in model_interv]
Crit_interv = [elem[4] for elem in model_interv]
R_interv = [elem[5] for elem in model_interv]
D_interv = [elem[6] for elem in model_interv]

S_interv_trace = get_trace_scatter(dates, S_interv, name='Susceptible', color='MediumBlue')
E_interv_trace = get_trace_scatter(dates, E_interv, name='Exposed', color='LightSkyBlue')
I_interv_trace = get_trace_scatter(dates, I_interv, name='Infected', color='GreenYellow')
Sev_interv_trace = get_trace_scatter(dates, Sev_interv, name='Severe', color='Orange')
Crit_interv_trace = get_trace_scatter(dates, Crit_interv, name='Critical', color='Red')
R_interv_trace = get_trace_scatter(dates, R_interv, name='Recovered', color='Pink')
D_interv_trace = get_trace_scatter(dates, D_interv, name='Deaths', color='Darkred')

traces_interv = [S_interv_trace, E_interv_trace, I_interv_trace,
                 Sev_interv_trace, Crit_interv_trace, R_interv_trace, D_interv_trace]



figure_interv = go.Figure(data=traces_interv, layout=layout)
figure_interv.update_layout(title='COVID19 | S-E-I-R epidemic model | Social Distancing',
                            xaxis=dict(
                                        rangeslider=dict(
                                            visible=True
                                        )))

#py.offline.plot(figure_interv, auto_open=True, show_link=False)

def current_plot():
    graphJSON = json.dumps(figure_interv, cls=py.utils.PlotlyJSONEncoder)

    return graphJSON