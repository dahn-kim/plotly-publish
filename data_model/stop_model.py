from data_model.formula_model import *
import json

#beta value for intervention stop : predicting when to stop the intervention
def beta_var1(t):
    if t > intervention_day and t < (intervention_day + 7):
        return beta*(1-intervention_rate)
    elif t > (intervention_day + 7) and t < end_day and t < (intervention_day + 16):
        return beta*(1-intervention_rate_later)
    elif t > (intervention_day + 16) and t < end_day:
        return beta*(1-intervention_rate_latest)
    else:
        return beta


#social distancing stopped after 3 weeks modelling
model_interv_stop = odeint(SEIR_model, [S, E, I, Severe, Crit, R, D], t,
                                           args=(beta_var1, latent, gamma, m_a, gamma_crit,
                                                 death_rate_normal, death_rate_crit,c_a, N, death_rate_severe))



# Plot Traces for Intervention Model incl. Stop
S_interv_stop = [elem[0] for elem in model_interv_stop]
E_interv_stop = [elem[1] for elem in model_interv_stop]
I_interv_stop = [elem[2] for elem in model_interv_stop]
Sev_interv_stop = [elem[3] for elem in model_interv_stop]
Crit_interv_stop = [elem[4] for elem in model_interv_stop]
R_interv_stop = [elem[5] for elem in model_interv_stop]
D_interv_stop = [elem[6] for elem in model_interv_stop]

S_interv_stop_trace = get_trace_scatter(dates, S_interv_stop, name='Susceptible', color='MediumBlue')
E_interv_stop_trace = get_trace_scatter(dates, E_interv_stop, name='Exposed', color='LightSkyBlue')
I_interv_stop_trace = get_trace_scatter(dates, I_interv_stop, name='Infected', color='GreenYellow')
Sev_interv_stop_trace = get_trace_scatter(dates, Sev_interv_stop, name='Severe', color='Orange')
Crit_interv_stop_trace = get_trace_scatter(dates, Crit_interv_stop, name='Critical', color='Red')
R_interv_stop_trace = get_trace_scatter(dates, R_interv_stop, name='Recovered', color='Pink')
D_interv_stop_trace = get_trace_scatter(dates, D_interv_stop, name='Deaths', color='Darkred')

traces_interv_stop = [S_interv_stop_trace, E_interv_stop_trace, I_interv_stop_trace,
                      Sev_interv_stop_trace, Crit_interv_stop_trace, R_interv_stop_trace, D_interv_stop_trace]



figure_interv_stop = go.Figure(data=traces_interv_stop,layout=layout)
figure_interv_stop.update_layout(title='COVID19 | S-E-I-R epidemic model | Intervention Stop',
                                  xaxis=dict(
                                    rangeslider=dict(
                                        visible=True
                                   )))


#py.offline.plot(figure_interv_stop, auto_open=True, show_link=False)

def stop_plot():

    graphJSON = json.dumps(figure_interv_stop, cls=py.utils.PlotlyJSONEncoder)

    return graphJSON