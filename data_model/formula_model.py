import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import scipy
from scipy.integrate import solve_ivp
from scipy.integrate import odeint

from pathlib import Path

import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html

import plotly as py
import plotly.graph_objs as go

from datetime import datetime

"""inital numbers and values"""

N = 8858775  # population of Austria

# Day zero of model: 15. March 2020

# Susceptible, Infected, Recovered, Exposed, Deaths
I = 1034
Hosp = I * 0.094  # hospitalized cases
Crit = Hosp * 0.228  # critical (ICU) patients from Hospitalized
Severe = Hosp - Crit  # severe cases
# Mild = I - Ser #mild/asymtoptic cases
m_a = 0.906  # fraction of mild cases AUT = 90.56
c_a = 0.228  # fraction of hospitalized turning critical
# exposed = infected[day = 0] - infected [day = -5.5] 112
E = 2907  # 6d ahead
D = 1
R = 6
S = (N - I - E - D - R)
# 12 to 32 days until recovery gamma = 1/D (d = 20)
# gamma = 0.125  # mean D = 8 days --> 1/D (viral shedding mild symptoms)
latent = 1 / 6  # median D = 6.4 d (or 5.5 - 0.18) --> 1/D
# global: 5.4% deaths
death_rate_crit = 0.1  # 10%
death_rate_normal = 0.01  # 1%
death_rate_severe = 0.054  # 5.4
# mean R0 = 3.28
R_basic = 3.28  # 3.28
# gamma = 0.0714 #mean days to recovery = 14 --> 1/D (0.0714) #21d - 0.048 #γ=1–exp(–1/dI )
gamma = 1 - np.exp(-1 / 14)
# gamma_crit = 0.029 #gamma for server cases: mean 4.5 weeks (32 d), 5w (35d) - 0.029
gamma_crit = 1 - np.exp(-1 / 32)
contact_factor = 2.25  # 13.5 contacts per day
beta = R_basic * gamma * contact_factor  # 0.234 #0.15744
# beta_no_intervention = R_basic*gamma
intervention_rate = 0.25
intervention_rate_later = 0.6
intervention_rate_latest = 0.75
R_intervention = beta * intervention_rate / gamma
# 1.64 --> reduction of beta by factor 0.5 --> 50% less social contact

end_day = 90  # end intervention after 50 days of isolation

days = 365  # Simulation period
t = np.linspace(1, days, days)  # time in days, approx. 4 months (March - June)

intervention_day = 40

datelist = pd.date_range(start="2020-03-13", periods=days)
dates = (datelist.date).tolist()

"""SEIR MODELING"""


def dS_dt(S, I, beta, N):
    return -beta * S * I / N


def dE_dt(S, E, I, beta, latent, N):
    return beta * S * I / N - latent * E


def dI_dt(I, E, latent, gamma):
    return E * latent - I * gamma


# severe cases
def dSev_dt(I, Severe, gamma_crit, gamma, m_a, death_rate_crit, Crit):
    return ((1 - m_a) * (I * gamma)) + ((1 - death_rate_crit) * Crit * gamma_crit) - (Severe * gamma_crit)


# critical cases
def dCrit_dt(Severe, Crit, gamma_crit, c_a):
    return (c_a * Severe * gamma_crit) - (Crit * gamma_crit)


# recovered cases
def dR_dt(I, gamma, m_a, Severe, gamma_crit, c_a):
    return (m_a * gamma * I) + (1 - c_a) * (Severe * gamma_crit)


# dead cases
def dD_dt(I, death_rate_normal, gamma, death_rate_crit, Crit, gamma_crit, Severe, death_rate_severe):
    return death_rate_normal * I * gamma + death_rate_crit * Crit * gamma_crit + death_rate_severe * Severe * gamma_crit


# (SEIR_model, [S, E, I, Ser, R, D], t, args = (beta, latent, gamma, m_a, gamma_crit, death_rate, N))
def SEIR_model(y, t, beta, latent, gamma, m_a, gamma_crit, death_rate_normal, death_rate_crit, c_a, N,
               death_rate_severe):
    if callable(beta):
        beta = beta(t)
    else:
        beta = beta

    S, E, I, Severe, Crit, R, D = y

    S_out = dS_dt(S, I, beta, N)
    E_out = dE_dt(S, E, I, beta, latent, N)
    I_out = dI_dt(I, E, latent, gamma)
    Sev_out = dSev_dt(I, Severe, gamma_crit, gamma, m_a, death_rate_crit, Crit)
    Crit_out = dCrit_dt(Severe, Crit, gamma_crit, c_a)
    R_out = dR_dt(I, gamma, m_a, Severe, gamma_crit, c_a)
    D_out = dD_dt(I, death_rate_normal, gamma, death_rate_crit, Crit, gamma_crit, Severe, death_rate_severe)

    return S_out, E_out, I_out, Sev_out, Crit_out, R_out, D_out


def get_trace_scatter(x, y, name=None, color=None):
    trace = go.Scatter(x=x,
                       y=y,
                       mode='lines',
                       name=name,
                       line=dict(color=color, width=2), opacity=0.8
                       )

    return trace


layout = go.Layout(template='none',
                   autosize=True,
                   showlegend=True,
                   title='COVID19 | S-E-I-R epidemic model',
                   xaxis_title='time [days]',
                   yaxis_title='Population',
                   updatemenus=[
                       dict(
                           type="buttons",
                           direction="left",
                           buttons=list([
                               dict(label="Complete Data",
                                    method="restyle",
                                    args=[{'visible': [True, True, True, True, True, True, True]}]),
                               dict(label="S | Susceptible",
                                    method="restyle",
                                    args=[{'visible': [True, False, False, False, False, False, False]}]),
                               dict(label="E | Exposed",
                                    method="restyle",
                                    args=[{'visible': [False, True, False, False, False, False, False]}]),
                               dict(label="I | Infected)",
                                    method="restyle",
                                    args=[{'visible': [False, False, True, False, False, False, False]}]),
                               dict(label="Severe Cases",
                                    method="restyle",
                                    args=[{'visible': [False, False, False, True, False, False, False]}]),
                               dict(label="Critical Cases",
                                    method="restyle",
                                    args=[{'visible': [False, False, False, False, True, False, False]}]),
                               dict(label="R | Recovered",
                                    method="restyle",
                                    args=[{'visible': [False, False, False, False, False, True, False]}]),
                               dict(label="Deaths",
                                    method="restyle",
                                    args=[{'visible': [False, False, False, False, False, False, True]}])
                           ]),

                           showactive=True,
                           # pad={"r": 0, "t": 0},
                           x=0,
                           xanchor="left",
                           # y=1,
                           yanchor="top",

                       )
                   ])
