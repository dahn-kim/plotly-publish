import pandas as pd
import numpy as np

from scipy.integrate import solve_ivp
import plotly.graph_objects as go

import pandas as pd
import plotly
import json

'''
SEIR Model functions for calculating S,E,I,R in respect of time.
Return dS(t),dE(t, dI(t), dR(t) over time (dT)
'''


def dS_dt(N, S, I, R_t, beta):
    return R_t * -beta * S * I / N


def dE_dt(N, S, E, I, R_t, beta, sigma):
    return R_t * beta * S * I / N - sigma * E


def dI_dt(I, E, sigma, gamma):
    return E * sigma - I * gamma


def dR_dt(I, gamma):
    return gamma * I


def SEIR_model(t, y, R_t, beta, sigma, gamma, N):
    if callable(R_t):
        reproduction = R_t(t)
    else:
        reproduction = R_t

    S, E, I, R = y

    S_out = dS_dt(N, S, I, reproduction, beta)
    E_out = dE_dt(N, S, E, I, reproduction, beta, sigma)
    I_out = dI_dt(I, E, sigma, gamma)
    R_out = dR_dt(I, gamma)

    return S_out, E_out, I_out, R_out


'''
Generate trace for two-dimensional plotting.
Return trace.
'''


def get_trace(t, X, name, c):
    trace = go.Scatter(x=t,
                       y=X,
                       mode='lines',
                       name=name,
                       line=dict(color=c, width=1)
                       )

    return trace


'''
Create traces for plot.
Return traces.
'''


def get_traces(model, days):
    S, E, I, R = model.y
    # convert S,E,I,R values to integers (full persons)
    l_S = []
    l_E = []
    l_I = []
    l_R = []
    for s, e, i, r in zip(S, E, I, R):
        l_S.append(int(s))
        l_E.append(int(e))
        l_I.append(int(i))
        l_R.append(int(r))

    # days = timespan and points defined by user in function
    t = np.linspace(0, days,
                    days + 1)  # time points for days defined. d+1 timepoints inbetween start and end to end up with whole number(int) timepoints

    # create traces for plotting function
    S_trace = get_trace(t, l_S, name='Susceptible', c='MediumBlue')
    E_trace = get_trace(t, l_E, name='Exposed', c='LightSkyBlue')
    I_trace = get_trace(t, l_I, name='Infected', c='GreenYellow')
    R_trace = get_trace(t, l_R, name='Recovered', c='LimeGreen')

    traces = [S_trace, E_trace, I_trace, R_trace]

    return S_trace, E_trace, I_trace, R_trace


def create_plot():
    # Parameters

    N = 6080000  # Population size
    days = 365  # Simulation period

    # Initial state
    E0 = 0
    I0 = 1
    R0 = 0
    S0 = N - E0 - I0 - R0

    beta = 0.25  # average transmission of 1 passing the virus to 2.2 individuals
    sigma = 0.143  # average infectious period 1/5
    gamma = 0.11  # average recovery period 1/10
    R_0 = 2.5  # reproduction number
    R_t = 0.75  # reproduction number after intervention
    interv_day = 100  # initial day of the measure being introduced

    '''
    Time dependency for interventions.
    Return reproduction number, based on time factor.
    '''

    def R_var(t):
        if t > interv_day:
            return R_t
        else:
            return R_0

    model_intervent = solve_ivp(SEIR_model, [0, days], [S0, E0, I0, R0], args=(R_var, beta, sigma, gamma, N),
                                t_eval=np.arange(days))

    mod_intervent_traces = get_traces(model_intervent, days)

    S = mod_intervent_traces[0]
    E = mod_intervent_traces[1]
    I = mod_intervent_traces[2]
    R = mod_intervent_traces[3]

    fig = go.Figure()

    fig.add_trace(S)
    fig.add_trace(E)
    fig.add_trace(I)
    fig.add_trace(R)

    # layout definition i.e. title, description of axes etc.
    fig.update_layout(template='none',
                      autosize=True,
                      showlegend=True,
                      title='Estimating COVID19 with the S-E-I-R epidemic model',
                      xaxis_title='time [days]',
                      yaxis_title='Population'
                      #width=800, height=800
                      )


    # Add drowdowns
    # button_layer_1_height = 1.08
    button_layer_1_height = 1.12
    button_layer_2_height = 1.065

    fig.update_layout(
        updatemenus=[
            dict(
                type="buttons",
                buttons=[
                    dict(label="S-E-I-R",
                         method="restyle",
                         args=[{'visible': [True, True, True, True]}]),
                    dict(label="S | Susceptible",
                         method="restyle",
                         args=[{'visible': [True, False, False, False]}]),
                    dict(label="E | Exposed",
                         method="restyle",
                         args=[{'visible': [False, True, False, False]}]),
                    dict(label="I | Infected)",
                         method="restyle",
                         args=[{'visible': [False, False, True, False]}]),
                    dict(label="R | Recovered",
                         method="restyle",
                         args=[{'visible': [False, False, False, True]}])
                ],
            )
        ]
    )

    # fig.update_layout(template='none',
    #                   showlegend=True,
    #                   title='COVID19 | S-E-I-R epidemic model',
    #                   xaxis_title='time [days]',
    #                   yaxis_title='Population',
    #                   width=800, height=800)

    # plotly.offline.plot(fig, auto_open=True, show_link=False)

    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    return graphJSON
