# Display data visualisation(Python and Plotly) using Flask and gitlab CI/CD

<p>This was a small university project during the COVID-19 crisis in April, 2020. This website (https://dahn-kim.gitlab.io/plotly-publish/) is a part of templates 
I created to publish our data analysis on COVID-19 prediction in Austria 
(prediction of another outbreaks and the importance and the impact of social distaning)
</p>
<p>
The final publication is published via herokuapp [https://infectious-disease-modeling.herokuapp.com/]
</p>
---

Example [Frozen-Flask](http://pythonhosted.org/Frozen-Flask/) website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---


